class Tondeuse:
    def __init__(self,position,instructions):#constructeur
        self.position=position #position de la tondeuse
        self.instructions=instructions #instructions que la tondeuse doit exécuter
        self.rotation={'G':-1,'D':1} #va nous permettre de calculer la nouvelle orientation de la tondeuse après une rotation
        self.decalages={'N':(0,1), 'S':(0,-1), 'E':(1,0), 'W':(-1,0)} # permettra le calcul des nouvelles coordonéées de la tondeuse après un déplacement
        self.C=['N','E','S','W'] # différentes orientations 
    def executeInstruction(self, instruction): #permet d'exécuter une instruction précise
        if self.isRotation(instruction): #s'il s'agit d'une rotation
            self.position['orientation']=self.getOrientation(self.position['orientation'], instruction) #on met A jour la nouvelle Orientation
        else: #sinon, on suppose que c'est un déplacement
            a,b=self.decalages[self.position['orientation']] #les différents décalages à faire sur chaque axe 
            #mise à jour des coordonnées
            self.position['cords']['x']+=a 
            self.position['cords']['y']+=b
            
    def getOrientation(self, ancienneOrientation, rotation):#calcul de la nouvelle orientaion
        return self.C[(self.C.index(ancienneOrientation)+self.rotation[rotation])%4] 
    
    def isRotation(self,instruction): #determine si l'instruction est une rotation
        return instruction =='G' or instruction=='D'
    def printPosition(self):
        print(str(self.position['cords']['x'])+
              ' '+str(self.position['cords']['y'])+
              ' '+str(self.position['orientation']))