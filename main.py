import sys
import Tondeuse, Ground
#fonction pour generer le terrain avec les tondeuses positionnées dessus grace au nom du fichier entré
def generateGround(file):
    with open(file,'r') as f:
        firstLine=f.readline().strip().split(' ')
        xMax, yMax=int(firstLine[0]), int(firstLine[1]) #coordonnées du coin supérieur droit du terrain
        line=f.readline()
        tondeuses=[] #liste des tondeuses sur le terrain
        while line: #on parcoure le fichier pour recuperer les tondeuses
            stringPosition = line.strip().split(' ')
            x,y, orientation=int(stringPosition[0]), int(stringPosition[1]), stringPosition[2] #position de chaque tondeuse
            instructions =f.readline().strip() #liste des instructions de la tondeuse
            tondeuses.append(Tondeuse.Tondeuse({'cords':{'x':x,'y':y},'orientation':orientation},
             instructions)) #ajout de la tondeuse à la liste 
            line=f.readline()
    return Ground.Terrain(tondeuses, xMax,yMax) #finalement on cree l'objet Terrain
fileName=sys.argv[1].strip('"\'') if(len(sys.argv)>=2) else 'test.txt' #récupération du nom du fichier passé en argument de ligne de commande
terrain=generateGround(file=fileName) #appel de la fonction pour generer le terrain
terrain.step() #mettre les tondeuses en marche séquentiellement