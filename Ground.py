import copy
class Terrain:
    def __init__(self, tondeuses, xMax, yMax):#initialisation
        #coordonnées du coin supérieur droit
        self.xMax=xMax
        self.yMax=yMax
        #liste des tondeuses sur le terrain
        self.tondeuses=tondeuses
    def isValidPosition(self, cords):#tester si une tondeuse n'est pas sortie des limites du terrain
        x,y=cords['x'], cords['y']
        return x>=0 and y>=0 and x<=self.xMax and y<=self.yMax
    def step(self):#mise en marche séquentielle des tondeuses
        for tondeuse in self.tondeuses: #pour chaque tondeuse
            self.runTondeuse(tondeuse) #executer toutes les instructions que la tondeuse doit executer 
            tondeuse.printPosition() #afficher la position de la tondeuse
            print('\n') #saut de ligne
    def runTondeuse(self, tondeuse): #permet à une tondeuse données d'exécuter toutes les instructions qu'elle devrait executer
        for i in range(len(tondeuse.instructions)): #pour chaque instruction
            position=copy.deepcopy(tondeuse.position) #stocker la position actuelle de la tondeuse; on utilise copy car les dictionaires sont mutables et la position est un dictionnaire 
            tondeuse.executeInstruction(tondeuse.instructions[i]) #executer l'instruction
            if not self.isValidPosition(tondeuse.position['cords']):# si on est sorti des limites du terrain
                tondeuse.position=position #on revient à la position précédente